package ratelimit

import (
	"github.com/caddyserver/caddy/v2/caddyconfig/caddyfile"
	"github.com/caddyserver/caddy/v2/caddyconfig/httpcaddyfile"
	"github.com/caddyserver/caddy/v2/modules/caddyhttp"
)

func init() {
	httpcaddyfile.RegisterHandlerDirective("matrix", parseCaddyfile)
}

// parseCaddyfile sets up a handler for rate-limiting from Caddyfile tokens. Syntax:
//
//     rate_limit [<matcher>] <key> <rate> [<zone_size> [<reject_status>]]
//
// Parameters:
// - <key>: The variable used to differentiate one client from another.
// - <rate>: The request rate limit (per key value) specified in requests per second (r/s) or requests per minute (r/m).
// - <zone_size>: The size (i.e. the number of key values) of the LRU zone that keeps states of these key values. Defaults to 10,000.
// - <reject_status>: The HTTP status code of the response when a client exceeds the rate. Defaults to 429 (Too Many Requests).
func parseCaddyfile(h httpcaddyfile.Helper) (caddyhttp.MiddlewareHandler, error) {
	rl := new(RateLimit)
	if err := rl.UnmarshalCaddyfile(h.Dispenser); err != nil {
		return nil, err
	}
	return rl, nil
}

func (rl *RateLimit) UnmarshalCaddyfile(d *caddyfile.Dispenser) (err error) {
	if d.Next() {
		args := d.RemainingArgs()
		if rl.MyMap == nil {
			rl.MyMap = make(map[string]string)
		}
		rl.MyMap[args[0]] = args[1]
	}
	return nil
}

// Interface guards
var (
	_ caddyfile.Unmarshaler = (*RateLimit)(nil)
)
